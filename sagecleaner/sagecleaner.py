__author__     = "Julien Simon"
__copyright__  = "Copyright 2019, Julien Simon"
__credits__    = ["Julien Simon"]
__license__    = "GPL"
__version__    = "0.0.4"
__maintainer__ = "Julien Simon"
__email__      = "julien@julien.org"
__status__     = "Prototype"

# SAGECLEANER: because sometimes, you just need to wipe it all out ;)

# Disclaimer : this is provided as-is.
# This scripts works for me, but if it destroys your infrastructure,
# sets your car on fire or breaks your marriage, it'll be your fault, not mine.

import argparse

import boto3
from boto3.session import Session
from botocore.exceptions import ClientError

s = Session()

resources = {
    'endpoints':
        {'name': 'Endpoints',
        'list_api': 'list_endpoints', 
        'list_api_response_level1': 'Endpoints', 
        'list_api_response_level2': 'EndpointName',
        'delete_api': 'delete_endpoint',
        'delete_api_parameter': 'EndpointName'},
    'endpoint_configs':
        {'name': 'Endpoint configurations',
        'list_api': 'list_endpoint_configs', 
        'list_api_response_level1': 'EndpointConfigs', 
        'list_api_response_level2': 'EndpointConfigName',
        'delete_api': 'delete_endpoint_config',
        'delete_api_parameter': 'EndpointConfigName'},
    'lifecycle_configs':
        {'name': 'Lifecycle configurations',
        'list_api': 'list_notebook_instance_lifecycle_configs',
        'list_api_response_level1': 'NotebookInstanceLifecycleConfigs',
        'list_api_response_level2': 'NotebookInstanceLifecycleConfigName',
        'delete_api': 'delete_notebook_instance_lifecycle_config',
        'delete_api_parameter': 'NotebookInstanceLifecycleConfigName'},
    'models':
        {'name': 'Models',
        'list_api': 'list_models',
        'list_api_response_level1': 'Models', 
        'list_api_response_level2': 'ModelName',
        'delete_api': 'delete_model',
        'delete_api_parameter': 'ModelName'},
    'notebook_instances':
        {'name': 'Notebook instances',
        'list_api': 'list_notebook_instances',
        'list_api_response_level1': 'NotebookInstances', 
        'list_api_response_level2': 'NotebookInstanceName',
        'stop_api': 'stop_notebook_instance',
        'stop_api_parameter': 'NotebookInstanceName',
        'delete_api': 'delete_notebook_instance',
        'delete_api_parameter': 'NotebookInstanceName'},
    'repositories':
        {'name': 'Repositories',
        'list_api': 'list_code_repositories',
        'list_api_response_level1': 'CodeRepositorySummaryList',
        'list_api_response_level2': 'CodeRepositoryName',
        'delete_api': 'delete_code_repository',
        'delete_api_parameter': 'CodeRepositoryName'}
 }

def region_exists(region):
    ec2 = boto3.client('ec2')
    response = ec2.describe_regions()
    region_names = [r['RegionName'] for r in response['Regions']]
    if region in region_names:
        return True
    else:
        print("Region %s does not exist or is not configured" % region)

def region_has_sagemaker(region):
    sagemaker_regions = s.get_available_regions('sagemaker')
    if region in sagemaker_regions:
        return True
    else:
        print("Region %s does not support Amazon SageMaker" % region)

########

parser = argparse.ArgumentParser(description='List and delete Amazon SageMaker resources')
group = parser.add_mutually_exclusive_group()

parser.add_argument('-r', '--regions', nargs='+', default=[], 
    help='list of AWS regions: region name(s), or all')
parser.add_argument('-l', '--list', nargs='+', default=[], 
    help='list resources: endpoints endpoint_configs lifecycle_configs models notebook_instances repositories, or all')
group.add_argument('-s', '--stop', action='store_true',
    help='stop notebook instances (ignored for other resources)')
group.add_argument('-d', '--delete', action='store_true',
    help='delete resources (no confirmation! you have been warned)')
parser.add_argument('-v', '--verbose', action='store_true', 
    help='print API responses')

args = parser.parse_args()
list_regions = args.regions
list_resources = args.list
delete_resources = args.delete
stop_resources = args.stop
verbose = args.verbose

if 'all' in list_regions:
    list_regions = s.get_available_regions('sagemaker')

if 'all' in list_resources:
    list_resources = resources.keys()

for region in list_regions:
    if region_exists(region) is not True:
        continue
    if region_has_sagemaker(region) is not True:
        continue

    print("Region %s" % region)
    sm = boto3.client('sagemaker', region_name=region)
    # Process resources
    for resource in list_resources:
        list_api = resources[resource]['list_api']
        list_api = getattr(sm, list_api)
        kwargs = { 'MaxResults': 100 }
        try:
            response = list_api(**kwargs)
        except ClientError as e:
                   print('        Error: %s' % e)

        if verbose:
            print('        %s' % response)
        response_level1 = response[resources[resource]['list_api_response_level1']]
        if response_level1:
            print('    %s (%d)' % (resources[resource]['name'], len(response_level1)))
        for resp in response_level1:
            response_level2 = resp[resources[resource]['list_api_response_level2']]
            print('        %s' % response_level2)

            # Stop notebook instances
            if resource == 'notebook_instances' and stop_resources:
                stop_api = resources[resource]['stop_api']
                stop_api = getattr(sm, stop_api)
                kwargs = { resources[resource]['stop_api_parameter']: response_level2 }
                try:
                    response = stop_api(**kwargs)
                except ClientError as e:
                    print('        Error: %s' % e)
                if verbose:
                    print('        %s' % response)

            # Delete resources
            if delete_resources:
               delete_api = resources[resource]['delete_api']
               delete_api = getattr(sm, delete_api)
               kwargs = { resources[resource]['delete_api_parameter']: response_level2 }
               try:
                   response = delete_api(**kwargs)
               except ClientError as e:
                   print('        Error: %s' % e)
               if verbose:
                   print('        %s' % response)

print('Done!')




